static char help[] = " ";

#include <petscdmplex.h>
#include <petscdmlabel.h>
#include <petscksp.h>


static PetscErrorCode zero(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  u[0] = 0;
  u[1] = 0;
  return 0;
}


int main(int argc, char **argv)
{
  DM             dm, dmDist = NULL;
  Vec            F;
  Mat            K;
  PetscSection   section, gs;
  PetscInt       dim = 2;
  PetscInt       Krow,Kcol,Fsize,id;
  PetscInt       v,vStart,vEnd;
  PetscErrorCode ierr;
  ISLocalToGlobalMapping  ltog;
  PetscDS        ds;

  ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
  PetscOptionsGetInt(NULL,NULL, "-dim", &dim, NULL);
  /* Create a mesh */
  DMPlexCreateBoxMesh(PETSC_COMM_WORLD, dim, PETSC_FALSE, NULL, NULL, NULL, NULL, PETSC_TRUE, &dm);

  /* Distribute mesh over processes */
  DMPlexDistribute(dm, 0, NULL, &dmDist);
  if (dmDist) {DMDestroy(&dm); dm = dmDist;}

  /* Get chart data */
  DMPlexGetHeightStratum(dm, 2, &vStart, &vEnd);

  /* Create Section */
  PetscSectionCreate(PETSC_COMM_WORLD, &section);
  PetscSectionSetNumFields(section, 1); 
  PetscSectionSetFieldComponents(section, 0, 2); 
  PetscSectionSetChart(section, vStart, vEnd);

  for (v = vStart; v < vEnd; ++v) {
    PetscSectionSetDof(section,v,2);
    PetscSectionSetFieldDof(section,v,0,2);
  }
  PetscSectionSetUp(section);
  DMSetLocalSection(dm, section);

  /* Assign BC */
  id = 1;
  DMCreateDS(dm);
  DMGetRegionNumDS(dm, 0, NULL, NULL, &ds);
  DMAddBoundary(dm, DM_BC_ESSENTIAL, "wall", "marker", 0, 0, NULL, (void (*)(void)) zero, 1, &id, NULL);

  DMGetGlobalSection(dm, &gs);
  PetscSectionView(gs,PETSC_VIEWER_STDOUT_WORLD);

  DMCreateMatrix(dm,&K);
  MatGetSize(K,&Krow,&Kcol);
  DMCreateGlobalVector(dm,&F);
  VecGetSize(F,&Fsize);
//  printf("%d \n",Krow);
//  printf("%d \n",Kcol);
//  printf("%d \n",Fsize); 
  DMGetLocalToGlobalMapping(dm,&ltog);
  ISLocalToGlobalMappingView(ltog,PETSC_VIEWER_STDOUT_WORLD);


  /* Cleanup */
  PetscSectionDestroy(&section);
  DMDestroy(&dm);
  MatDestroy(&K);
  VecDestroy(&F);
  PetscFinalize();
  return ierr;
}
